FROM python:3.7

MAINTAINER Daniel Juarez Gonzalez <djuarezg@cern.ch>

# python-novaclient for openstack management
# openshift for checking running pods
RUN pip install python-novaclient openshift

COPY cleanup.py /

ENTRYPOINT ["python", "/cleanup.py"]
