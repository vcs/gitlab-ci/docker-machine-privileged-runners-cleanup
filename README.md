# Docker-machine cleanup

This project stores a Python image required to cleanup any error or dangling VM that are spawned by `oo-gitalb-cern` deployed GitLab-runners.

## Behaviour

This image will run in a scheduled job that performs the following actions:

1. Checks for any virtual machines running on a given Openstack tenant that use a given image.
1. For all retrieved servers, it deletes any failed VM, no matter how or from where it spawned.
1. According to metadata options checks for VMs that correspond to the given Openshift namespace and delete those whose parent GitLab-runner pod does not exist anymore on the namespace.
