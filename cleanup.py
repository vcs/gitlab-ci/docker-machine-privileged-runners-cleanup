import os
import yaml
import sys
from keystoneauth1 import loading
from keystoneauth1 import session
from novaclient import client as novaclient
from datetime import datetime, timedelta
from kubernetes import client as k8sclient, config
from openshift.dynamic import DynamicClient


POD_NAMESPACE = os.environ["POD_NAMESPACE"]
CI_SERVER_URL = os.environ["CI_SERVER_URL"]
OS_USERNAME = os.environ['OS_USERNAME']
OS_PASSWORD = os.environ['OS_PASSWORD']
OS_TENANT_ID = os.environ['OS_TENANT_ID']
OS_AUTH_URL = "https://keystone.cern.ch/v3"
VERSION = "2.1"
METADATA_KEY = "docker-machine-runners-cleanup"

# Openshift inCluster configuration
config.load_incluster_config()
v1 = k8sclient.CoreV1Api()

try:
    # Openstack configuration
    loader = loading.get_plugin_loader('password')
    auth = loader.load_from_options(auth_url=OS_AUTH_URL,
                                    username=OS_USERNAME,
                                    password=OS_PASSWORD,
                                    project_id=OS_TENANT_ID,
                                    user_domain_name="Default")
    sess = session.Session(auth=auth)
    nova = novaclient.Client(VERSION, session=sess)

    # Retrieve VMs with METADATA_KEY. We cannot use search_opts with metadata.
    servers = nova.servers.list()
    cleanup_servers = [server for server in servers if METADATA_KEY in server.metadata and server.metadata[METADATA_KEY] == "true"]

    print("There are %s CoreOS servers configured for cleanup running in the tenant" % len(cleanup_servers))
    for coreos_server in cleanup_servers:
        # Existing pods on the current Openshift namespace. We retrieve them every time, otherwise the pods might have changed in the meantime
        # We do not care about the status of the pod
        existing_pods = v1.list_namespaced_pod(namespace=POD_NAMESPACE)
        existing_pods_names = [o.metadata.name for o in existing_pods.items]

        # Clean failed VMs in all cases
        if coreos_server.status == "SHUTOFF" or coreos_server.status == "ERROR":
            print("%s is in %s state, deleting..." % (coreos_server.name,coreos_server.status))
            coreos_server.delete()

        # Delete VMs that do not have metadata referencing any existing pod on the current environment
        elif (coreos_server.metadata["gitlab_env"] == CI_SERVER_URL) and (not coreos_server.metadata["runner_pod"] in existing_pods_names):
            print("%s is not referenced by any existing pod on %s, deleting..." % (coreos_server.name,CI_SERVER_URL))
            coreos_server.delete()

except Exception as e:
    print("There was an error during the cleanup, will retry on the next run")
    sys.exit(str(e))

print("Finished cleaning up docker-machine servers")